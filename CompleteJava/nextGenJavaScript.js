//----------------
//Let and Constant
//----------------
//Es6 replaces var with let and const - where let is used in place of var and for never changing values

var myName2 = "aryan2"
console.log(myName2)

let myName = "aryan"
console.log(myName)


//----------------
//Arrow Functions
//----------------

function myFunc(param) {
    console.log(param)
}

const myFunc2 = (param) => {
    console.log(param)
}

let myFunc3 = (param) => {
    console.log(param)
}

//can use let or const - again when you wanna change the function assigned to a name use let,
//if not use const

//----------------
//Import and Export
//----------------

//1. Default Export/Import
//importing the whole class file
// --->  import className from "path"
//export the class
// ----> export default className

//2. Named Export/Import
//importing a function or a const/let from a file
// --->  import {constName} from "path"
//export the function/const/let
// ----> export const constName = value
// || or ||
// ----> export const funcName = () => {....}


//----------------
//Classes
//----------------


class Human {
    constructor(props) {
        this.gender = 'male'
    }
    //better way to declare a function in class
    printGender = () => this.gender
}

class Person extends Human{
    constructor(props) {
        super(props)
        this.name = "aryan"

    }
    //better way to declare class variables, without constructor required
    //super and constructor both not required in es7
    //lastName = "arora"
    printName = () => {
        console.log(this.name)
    }
}

let myPerson = new Person()
myPerson.printName()
myPerson.printGender()


//----------------
//Spread and Rest Operators
//----------------

//Spread
//helps copying all the elements, deep copy
//add new elements after the copy by defining them after a comma
//spread is generally used to split up the elements of an array or object and then add more is necessary,
// thereafter creating the new array or object
var number = [1,2,3]
var newNumber = [...number, 7]

var person = new Person()
var newPerson = {...person, age : 25}//new var/property is added after comma
//if property/var already exists in the person object, it is over-ridden by the new one defined by the spread operator


//Rest
//merges all the elements in an array

function sort(...arg) {
    return arg.sort()
}

const filter = (...args) => {
    return args.filter(item => { //args now being an array, we can use build in methods of array on args
        if(item === 1){
            return true
        }
    })
}

filter(1,2,3,4) //merges all the passed arguments in a single array


//----------------
//Destructuring
//----------------

//helps to extract out elements from the object or array
//same as spread although spread takes out the whole array/object,
//destructuring however takes/extracts out only single element/properties/functions etc

//syntax -
// Array - [var1,var2] = array , var1 and var2 gets value of first and second element of the array
// Object - {propertyName1, propertyName2} = {propertyName1 : "value1", propertyName2 : "value2"}

let number1 = [1,2,3]
let [num1, num2] = number1 //here num1 and num2 are assigned the value of first and second element respectively from the number1 array

//if num2 not required, simply leave it blank and get num3
let [num1, ,num3] = number1

console.log(num1,num2)

let {name, age} = {name : "aryan", age : 21}//propertyName should always match in both
console.log(name, age)

