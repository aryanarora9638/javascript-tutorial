//https://www.youtube.com/watch?v=t2CEgPsws3U
//Tutorial code
{
    // Creates a stack
    var Stack = function() {
        this.count = 0;
        this.storage = {};

        // Adds a value onto the end of the stack
        this.push = function(value) {
            this.storage[this.count] = value;
            this.count++;
        }

        // Removes and returns the value at the end of the stack
        this.pop = function() {
            if (this.count === 0) {
                return undefined;
            }

            this.count--;
            var result = this.storage[this.count];
            delete this.storage[this.count];
            return result;
        }

        this.size = function() {
            return this.count;
        }

        // Returns the value at the end of the stack
        this.peek = function() {
            return this.storage[this.count-1];
        }
    }

    var myStack = new Stack();

    myStack.push(1);
    myStack.push(2);
    console.log(myStack.peek());
    console.log(myStack.pop());
    console.log(myStack.peek());
    myStack.push("freeCodeCamp");
    console.log(myStack.size());
    console.log(myStack.peek());
    console.log(myStack.pop());
    console.log(myStack.peek());

}


//good method to use
function Stack() {
    this.count = 0;
    this.list = [];

    this.push = function (value) {
        this.count++;
        this.list[this.count] = value;
    }
    
    this.pop = function () {
        var removeValue = this.list[this.count];
        delete this.list[this.count];
        this.count--;
        return removeValue;
    }
    
    this.peek = function () {
        if(this.count === 0){
            return undefined;
        }
        return this.list[this.count];
    }
    
    this.size = function () {
        return this.count;
    }
}

//never use this method, unless object created only once ever
var Stack_2 = {
    count: 0,
    list: [],
    push: function (value) {
        this.count++;
        this.list[this.count] = value;
    },
    pop: function () {
        var removeValue = this.list[this.count];
        delete this.list[this.count];
        this.count--;
        return removeValue;
    },
    size: function () {
        return this.count;
    }

}

//good method to use, same as the first one
var Stack_3 =  function () {
    this.count = 0;
    this.list = [];

    this.push = function (value) {
        this.count++;
        this.list[this.count] = value;
    }

    this.pop = function () {
        var removeValue = this.list[this.count];
        delete this.list[this.count];
        this.count--;
        return removeValue;
    }

    this.peek = function () {
        if(this.count === 0){
            return undefined;
        }
        return this.list[this.count];
    }

    this.size = function () {
        return this.count;
    }

}


//#1 method
var myStack = new Stack();
myStack.push(12);
myStack.push(11);
myStack.push(10);
console.log("Stack is - " + myStack.size())
console.log(myStack.pop());
console.log(myStack.pop());
console.log(myStack.pop());

//#2 method
var myStack_2 = Stack_2;
myStack_2.push(12);
myStack_2.push(11);
myStack_2.push(10);

var myStack_3 = Stack_2;
console.log("Stack is - " + myStack_3.size());
console.log(myStack_3.pop());
console.log(myStack_3.pop());
console.log(myStack_3.pop());

console.log("Stack is - " + myStack_2.size());
console.log(myStack_2.pop());
console.log(myStack_2.pop());
console.log(myStack_2.pop());

//#3 method
var myStack_4 = new Stack_3();
myStack_4.push(10);
myStack_4.push(11);
myStack_4.push(12);
console.log("Stack is - " + myStack_4.size());
console.log(myStack_4.pop());
console.log(myStack_4.pop());
console.log(myStack_4.pop());

