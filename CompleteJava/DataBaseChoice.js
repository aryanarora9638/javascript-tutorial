//Relational vs Non-Relational DB
//eg - SQL vs MongoDB

/* == SQL  - STRUCTURED QUERY LANGUAGE == */

// 1. Data uses schema
//Good - if you want to have a structured schema,or a predictable layout
//Bad - if you want to be flexible in storing your data

// 2. Relations & 3. Data distributed across several tables
//Good - if you have data that changes frequently and requires updating at only one place.
// And all the queries will get that updated info.
//Bad - if you have complex queries and lots of reads can be very hectic and coz speed delays

// 4. Scaling
//Good - Vertical scaling (computational power to the same server/db) is possible but still has limits
//Bad - Horizontal scaling (data to be distributed across several servers/db) is very difficult of a server/db.
//since all the tables are related/linked, horizontal scaling is very difficult.

// 5. Read and write
//Bad - Limitations for lots of (thousands) read and write queries,
// simply because data can be structured or related in a very complex manner.


/* == MongoDB == */

// 1. Schema Less
//Good - more flexible in storing the data
//Bad - can't rely on ur data to have a specific field

// 2. No Relations
//Good - fast when you have lots of read than write
//Bad - writing can lead to updating several tables/collections

// 3. Data is merged or nested in few collections
 //Good - some collections are merged which are read alot all the time,
// merged in a way so it has part of the data from all collections it needs
//Bad - updating is the problem, since has to be updated in several collections

// 4. Scaling
//Good - Both horizontal and vertical scaling is possible

// 5. Read and write
//Good - Read is very fast, since all the required data is already merged in one collection
//Bad - write is slow, since requires to update several collections