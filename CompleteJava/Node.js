var http = require('http');
var customeDateModule = require('./dateModuleTest.js');
var url = require('url');

//FileSystem
/*
    Read files
    Create files
    Update files
    Delete files
    Rename files
*/
var fs = require('fs');

var port = 8080;
http.createServer(function (req, res) {

    // body...
    console.log("server access on port: " + port);
    console.log("requested url - " + req.url);

    //Custom Module
    res.write(" \n Initial Message");
    res.write(' \n Hello World' + customeDateModule.myDateTime());

    if(req.url == '/'){
        console.log("basic request");
    }
    else{
        console.log("custom request");
        // var q = url.parse(req.url, true).query;
        // var text = q.year + " " + q.month;

        // res.write(text);
    }

    //fs.readFile('indexTest.html', function(err,data){
    //     if(err) throw err;
    //     res.writeHead(200, {'content-type': 'text/html'});
    //     res.write(data);
    //     res.end();
    // });

    var q = url.parse(req.url, true);
    var filename = "." + q.pathname;
    console.log(q.pathname);

    fs.readFile('indexTest.html', function (err, data) {
        // body...
        if(err) throw err;
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(data);
        res.end();
    });


}).listen(port);



//====================================

exports.myDateTime = function () {
    return Date();
};



//Rest Api
{
    //api is the contract provided by one piece of software to another piece of software
    //consists of req and res
    //api is the messenger between running software's and rest let's us format these requests
    //rest uses protocols eg http
    //rest api helps us format these requests from http

    //http methods
    //get - retrieve data from the server
    //post - add data to the server
    //put - update already added(posted) data to the server
    //delete - delete already added(posted) data from the serve

    //basic workflow
    //http request from client/user to the server/internet
    //api takes this request
    //and get's a response from the server, db whatever
    //passes the response to the http res, which is then passed on to the client

    //API can return data that you need for your application in a convenient format
    //Rest - data presentation for a client in the format that is convenient for it


}

