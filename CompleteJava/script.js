//====================================
document.write("Good Afternoon");
/*
document - object that relates to the html file its connected/linked to, represents the entire web page
write - method of document object
text - parameter for the method
*/


//====================================
//Variables

window.onload = function () {
    const name = "Aryan212";
    const age = 20;

    const previousName = document.getElementById("nameField");
    previousName.textContent = name;
    previousName.innerText = name;
    const previousAge = document.getElementById("age");
    previousAge.textContent = age.toString();


    const previousPrice = document.getElementById("price");
    previousPrice.innerHTML = '<a href = \" new message for price \" > new price people </a>';

}
//imp - document.getElementById - get the html element, and be used to alter the html fields
//fields altered by .textContent or .innerHTML where the former takes string value only and the later takes html code


//====================================
//Arrays

window.onload = function () {
    //two ways to declare an array in JS
    //array can multiple data types of data
    //finding the length of the array using .length keyword
    var firstArray;
    firstArray = [2,3,4,5,6];
    firstArray[0] = 10;

    var secondArray = new Array(2,3,4,5,6);
    secondArray[0] = 10;

    var differentArray = new Array(2,"two", 2.2, "t");
    differentArray[0] = 22;


    document.getElementById("arrayOne").innerText = firstArray[0].toString();
    document.getElementById('arrayTwo').innerText = secondArray[0].toString();

    document.getElementById("arrayOne").style.marginTop = "1em";
    document.getElementById("arrayOne").style.marginBottom = "1em";

    console.log(firstArray.length);
    console.log(secondArray.length);
    console.log(differentArray.length);

}


//====================================
//Functions, Methods and objects

window.onload = function () {
    //FUNCTIONS
    //declaring a function
    //data type of the parameters not required
    function nameOfFunction(param_a, param_b) {
        return param_a*param_b;
    }

    var area = nameOfFunction(3,2);
    //function can be called before or after writing the function
    //since the interpreter goes through the entire code before executing each line
    //thus it knows if and where the function exits.

    //function expression, also called anonymous functions since they do no have a name
    //the interpreter is expecting an expression but gets a function
    //this function is called a function expression
    var area_2 = function (param_a, param_b) {
        return param_a*param_b
    }
    var size = area_2(3,2);

    //Immediately invoked function expression - IIFE
    //parenthesis after the curly braces indicate to execute the function immediately
    //parenthesis wrapping the whole function ensures the interpreter treats this whole as an expression
    var area_3 = (function (param_a, param_b) {
        return param_a*param_b;
    }());
    //both IIFE and anonymous functions are used when the code has to be executed only once in the whole program


    //scope
    //local vs global scope
    //var inside the function are in local scope, while outside the function are in global scope
    //global scope variables takes more space since the interpreter has to track them all throughout the code



    //OBJECTS AND METHODS
    //functions that belong to an object are called methods
    //ways of declaring an object

    //#1
    //creating a single object
    //based on key value pairs
    var hotel = {
        name: "hotelName",
        rooms: 40,
        booked: 50,
        checkAvailability : function () {
            return this.rooms - this.booked;
        }
    }
    //adding a new property to the object
    hotel.newProperty = 25;

    //deleting a property
    delete hotel.newProperty;

    //accessing the object using the dot operator
    var hotelName = hotel.name;
    //or (since based on key value pairs)
    var hotelNamet = hotel['name'];

    var hotelRooms = hotel.rooms;
    //or
    var hotelRoomst = hotel['rooms'];

    var hotelBooked = hotel.booked;
    //or
    var hotelBookedt = hotel['booked'];


    //#2
    //creating object using the constructor
    var hotel_2 = new Object();
    //or
    //var hotel_2 = {}

    //adding properties to the object
    hotel_2.name = "newName";
    hotel_2.rooms = 25;
    hotel_2.booked = 40;

    hotel_2.checkAvailability = function () {
        return this.rooms - this.booked;
    }


    //#3 - IMP and most used
    //creating many objects
    //object declaration
    function hotelMakes(name, rooms, booked) {
        this.name = name;
        this.rooms = rooms;
        this.booked = booked;
        this.checkAvailability = function () {
            return this.rooms - this.booked;
        }
    }
    //instantiation the object
    var hotel_3 = new hotelMakes("newName", 25,40);
    var hotel_4 = new hotelMakes("newName2", 30, 35);
    var isAvailable = hotel_3.checkAvailability();
    var isAvailable_2 = hotel_4.checkAvailability();




    //the this operator
    //default object in scope is the window object of the page
    console.log(this.innerHeight);
    console.log(this.innerWidth);
    //here this refers to the window objects and furthermore the window object height and width

    //imp example
    //#1
    var width_1 = 600;
    var shape = {
        width_1 : 300
    }
    var showWidth = function () {
        return this.width_1;
    }
    //here showWidth() return the width_1 of the window/global scope and not the shape object scope.
    //but if
    shape.showWidth_2 = function () {
        return this.width_1;
    }
    //here the showWidth_2 returns the width_1 of the shape object, since here the showWidth belongs to the shape object and not the window/global


    //built-in object
    //Browser Object Model, Document object model, JS global object model

    //#1 - Browser Object model
    //contains the window object
    //window object contains several other objects, each with different set of properties
    //webpage -> Browser Object model -> window -> document, history, location, navigation, screen
    //more eg - refer to page 124
    window.document.getElementById("idk");
    window.history.back();
    window.location.bold();
    window.navigator;
    window.screen.width;
    //imp
    window.alert("alert");
    window.open("");
    window.print();

    //#2 - Document Object model (DOM)
    //collects data about the document from the html
    //more eg - refer to page 126
    document.getElementById("yo");
    document.lastModified;

    //#3 - Global JS objects
    //all the global variables in the script
    //as learned above
    //more eg - refer to page 128
    hotel.rooms.toUpperCase();

    //Decimal Numbers
    var num = 10.734982738572893;
    console.log(num.toFixed(3));//only 3 digits after decimal
    console.log(num.toPrecision(3))//3 digit precision
    console.log(num.toExponential(3))//returns in expo. notation

    //math object
    Math.pow(2,3);
    Math.floor(Math.random()*10 + 1);//random between 1 and 10

    //Date Object
    var today = new Date();
    today.getDate();
    today.getHours();
    //more eg - refer to page 137
}


//====================================
//Decision and Loops

window.onload = function () {
    //comparison Operator
    // == vs ===

    // ==
    //compares the value only
    // ===
    //compares value and data type

    //similarly != vs !==
    // != compares value only
    // !== compares value as well as the datatype

    //logical operator
    // &&, ||, !
    // && - logical and
    // || - logical or
    // ! - logical not


    //Switch statements
    //alternatives to multiple if statements as it saves the interpreter to check all the if's
    //since it breaks the code after a valid/true case has been found
    //like if, else if , else if, else
    //like else in the above code, default executes in the switch statements in case none is true
    var msg = ""
    var level = 2;
    //level here is the switch value, cases depend on it
    switch (level) {
        case 1:
            msg+="level - " + level;
            break; //break out of only this switch code and continue ot the rest of the code

        case 2:
            msg+="level - " + level;
            break;

        case 3:
            msg+="level - " + level;
            break;

        default:
            msg+="default case, level - " + level;
            break;
    }


    //IMP, New concept

    //Type Coercion - js tries to convert data types behind the scene to complete the operation.
    //eg - '1' > 0 is true, since '1' will be converted to 1 to perform the operation
    //this can lead to very unexpected results,
    //thus instead of == & != use === & !==

    //js uses weak typing since the var can change in datatype depending upon the value
    //some lang use strong typing as they  require one var to have a predefined fixed datatype

    //Truthy and Falsy Values
    //truthy - signifies presence, something valid, eg - var x = true, 1, 'any word', 10/5, 'true', '0', 'false'
    //falsy - signifies absence, something in valid, eg - var x = false, 0, '', 10/'word'
    //their use - if(truthy) {} else {}

    //thus instead of == & != use === & !==
    //else weird things will show as true, eg - false == 0, false == '', 0 == ''

    //some imp special cases
    //although null and undefined are both falsy, they are not equal to anything other than themselves
    // => null == undefined is true, while null == 0, null == false is all false
    //although NaN is falsy, but its not equal to anything, not even itself

    //Short Circuit Values
    //logical operator are processed left to right. They short circuit(stop) as soon as they have a value
    //but return the value that stopped the processing
    var artist = "aryan"
    var artistCopy = (artist || 'unkown'); //artist stopped the processing so, artistCopy has the value of artist

    artist = ''
    artistCopy = (artist || 'unkown'); ////artist did not stopped the processing so, artistCopy has the value of 'unkown'
    //as soon as the truthy value is found the rest of the options are not checked


    //Loops
    //for loop
    for(var i = 0; i < 10; i++){
        console.log(i);
    }
}


//====================================
//DOM - document object model

window.onload = function () {
    //DOM
    //describe how browser should create a model of an html page and how the js can access and update the contents
    //dom is neither the part of the html or the js

    //Dom tree
    //the model of the web page that loads is called the dom tree
    //dom tree has 4 main nodes - document, element, attribute, text node
    //Document node - point of access to the dom tree
    //Element node - all the html tags
    //Attribute node - property of the html tags
    //Text node - content/text of the html tags

    //Working with the dom tree
    //2 steps to dom - Locate/access then update/change

    //#1 Locating/accessing
    //selecting single, multiple elements and traversing the elements
    //nodeList - all the geElementBy___ returns a node list with is just a collection of all the nodes
    //can be accessed like an array using [index] or .items(index)
    //live nodeList - when the script updates the nodeList is also updated at the same time, faster to generate
    //static nodeList - when the script updates the nodeList is not updated, slower to generate

    //getElementById('id name');
    var x = document.getElementById("idName");

    //querySelector('css selector');
    //selects only the first one of the matching query
    //returns a static nodeList
    x = document.querySelector('htmlTagName.className');

    //getElementByClassName('class name');
    x = document.getElementsByClassName("className");
    var y = x.item(2);
    //or
    y = x[2];

    //getElementByTagName('tagName');
    x = document.getElementsByTagName("li");
    y = x[2];

    //querySelectorAll('css selector');
    //selects  all of the matching query
    //returns a static nodeList
    x = document.querySelectorAll('htmlTagName.className');
    y = x[2];


    //-----css selectors
    //Element - htmlTagName{ attributeName: value }
    //ID - #idName{ attributeName: value }
    //Class - .className{ attributeName: value }
    //Star(selects all) - *{ attributeName: value }
    //Descendant(selects child's) - parent child{ attributeName: value }
    //Adjacent(select siblings)  - brother+brother{ attributeName: value }
    //Attribute - htmlTag[attributeName : value]{}
    //nth of type htmlTagName:nth-of-type(index){}

    //Traversing the DOM
    //common method - parentNode, firstChild, LastChild, previousSibling, nextSibling
    x = document.getElementById("idName");
    x = x.firstChild.nextSibling;
    y = x.lastChild.previousSibling;
    var z = y.parentNode;

    //problem - traversing the dom can be difficult since many browsers add a whitespace between the nodes
    //solution - 1. use jquery, 2. no spaces between elements in html & 3. closing tags are put next to the opening tags
    //solution 2nd is good but makes the readability of the code very bad, thus 3rd is a better choice while 1st is best
    //eg - solution2
    //<ul><li>content1</li><li>content2</li><li>content3</li><li>content4</li><li>content5</li></ul>
    //eg - solution3
    /*
    <ul
    ><li>content1</li
    ><li>content2</li
    ><li>content3</li
    ><li>content4</li
    ><li>content5</li
    ></ul>
     */


    //Updating/changing the element
    //Three methods - nodeValue, innerHtml, textContent, innerText

    //nodeValue
    //problem is you have to be in the exact text node of the element, so getting to the element isn't sufficient
    x = document.getElementById("idName").firstChild.nextSibling.nodeValue;

    //innerText
    //this encounters the problem faced by innerValue
    //problem - obeys css rules. so might no show the values of text maybe hidden by css, performance is slow
    //therefore avoid using it
    x = document.getElementById("idName").innerText;

    //textContent
    //this encounters the problem faced by innerValue
    //gets entire text content from the element, including any markups or children
    x = document.getElementById("idName").textContent;

    //innerHtml
    //access to contents of the elements including any child elements
    x = document.getElementById("idName").innerHTML;
    x = '<li>test</li>' + x;


    //Adding Elements using DOM manipulation
    //Technique involves the use of - createElement(), createTextContent(), appendChild();
    //much better than innerHTML and safer
    var element = document.createElement('li');
    element.createAttribute('class', 'className');
    var text = document.createTextNode("testData");
    element.appendChild(text);

    var position = document.getElementById("idName")[2];
    position.appendChild(element);
    position.removeChild(element);


    //Attribute Node
    //methods - hasAttribute(), getAttribute(), setAttribute(), removeAttribute()
    var boolean = x.hasAttribute("class");
    if(boolean){
        var attr = x.getAttribute("class");
        x.setAttribute("class", "cool");
        x.removeAttribute("class");
    }
}


















































